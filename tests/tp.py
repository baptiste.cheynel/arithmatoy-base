# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return n*"S"+"0"


def S(n: str) -> str:
    return "S"+ n


def addition(a: str, b: str) -> str:

    count_a = len(a) - 1  # -1 pour ignorer le '0' final
    count_b = len(b) - 1  # de même pour b

    return 'S' * (count_a + count_b) + '0'


def multiplication(a: str, b: str) -> str:

    count_a = len(a) - 1
    count_b = len(b) - 1
    
    return 'S' * (count_a * count_b) + '0'


def facto_ite(n: int) -> int:

    r = 1
    
    for i in range(1,n+1):
        print (i,r)
        r = r * i
        
    return r


def facto_rec(n: int) -> int:
    if n == 0:  # base case
        return 1
    else:
        return n * facto_rec(n-1)  # recursive case




def fibo_rec(n: int) -> int:
    if n == 0: 
        return 0
    elif n == 1: 
        return 1
    else: 
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    a, b = 0, 1
    for _ in range(n):
        a, b = b, a + b
    return a


def golden_phi(n: int) -> float:
    return fibo_ite(n+1) / fibo_ite(n)

def sqrt5(n: int) -> float:
    approx = 2.0
    for _ in range(n):
        approx = 0.5 * (approx + 5 / approx)
    return approx

def pow(a: float, n: int) -> float:
    result = 1.0
    for _ in range(n):
        result *= a
    return result
