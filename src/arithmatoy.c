#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#define MAX(a,b) (((a)>(b))?(a):(b))

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }


char* arithmatoy_add(int base, const char* lhs, const char* rhs) {
    if (VERBOSE) 
    {
        printf("add: entering function\n");
    }

    int len1 = strlen(lhs);
    int len2 = strlen(rhs);
    int len = MAX(len1, len2) + 1;  

    char* result = (char*)calloc(len + 1, sizeof(char));  

    int carry = 0;
    for (int i = 0; i < len; ++i) {
        int digit1 = i < len1 ? get_digit_value(lhs[len1 - 1 - i]) : 0;
        int digit2 = i < len2 ? get_digit_value(rhs[len2 - 1 - i]) : 0;

        int sum = digit1 + digit2 + carry;
        result[len - 1 - i] = to_digit(sum % base);

        carry = sum / base;

        if (VERBOSE) 
        {
            printf("add: digit %d digit %d carry %d\n", digit1, digit2, carry);
            printf("add: result: digit %c carry %d\n", result[len - 1 - i], carry);
        }
    }

    // Remove leading zeros from the result
    const char* trimmed_result = drop_leading_zeros(result);
    strncpy(result, trimmed_result, len+1);

    return result;
}







char* arithmatoy_sub(unsigned int base, const char* lhs, const char* rhs) {
    int len1 = strlen(lhs);
    int len2 = strlen(rhs);
    int len = MAX(len1, len2);  

    char* result = (char*)calloc(len + 2, sizeof(char));  
    for (int i = 0; i < len; ++i) {
        result[i+1] = '0';  
    }

    int borrow = 0;
    for (int i = 0; i < len; ++i) {
        int digit1 = i < len1 ? get_digit_value(lhs[len1 - 1 - i]) : 0;
        int digit2 = i < len2 ? get_digit_value(rhs[len2 - 1 - i]) : 0;

        if (digit1 < digit2 + borrow) {
            result[len - i] = to_digit(digit1 + base - digit2 - borrow);
            borrow = 1;
        } else {
            result[len - i] = to_digit(digit1 - digit2 - borrow);
            borrow = 0;
        }
    }

    if (borrow > 0) {
        
        result[0] = '-';
    } else {
        memmove(result, result + 1, len + 1);  
    }

    
    const char* trimmed_result = drop_leading_zeros(result);
    if (trimmed_result != result) {
        memmove(result, trimmed_result, strlen(trimmed_result) + 1);
    }

    return result;
}



char* arithmatoy_mul(unsigned int base, const char* lhs, const char* rhs) {
    int len1 = strlen(lhs);
    int len2 = strlen(rhs);
    int len = len1 + len2;  

    char* result = (char*)calloc(len + 1, sizeof(char));  
    for (int i = 0; i < len; ++i) {
        result[i] = '0';  
    }

    for (int i = 0; i < len1; ++i) {
        int carry = 0;
        int n1 = get_digit_value(lhs[len1 - 1 - i]);
        for (int j = 0; j < len2; ++j) {
            int n2 = get_digit_value(rhs[len2 - 1 - j]);

            int sum = get_digit_value(result[i + j]) + n1 * n2 + carry;
            result[i + j] = to_digit(sum % base);
            carry = sum / base;
        }
        if (carry > 0) {
            result[i + len2] = to_digit(get_digit_value(result[i + len2]) + carry);
        }
    }

    reverse(result);  

    // Remove leading zeros from the result
    const char* trimmed_result = drop_leading_zeros(result);
    if (trimmed_result != result) {
        memmove(result, trimmed_result, strlen(trimmed_result) + 1);
    }

    return result;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
